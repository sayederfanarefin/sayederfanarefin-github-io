---
layout: post
date: 2021-6-22 15:59:00-0400
inline: true
---

Paper Published: Deep Neural Exposure - You Can Run, But Not Hide Your Neural Network Architecture!
Url to the paper: https://dl.acm.org/doi/abs/10.1145/3437880.3460415
