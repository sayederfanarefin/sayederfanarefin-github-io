---
layout: post
date: 2022-12-22 15:59:00-0400
inline: true
---

Paper Published: Weaponizing IoT Sensors: When Table Choice Poses a Security Vulnerability
Url to the paper: https://ieeexplore.ieee.org/abstract/document/10063566
