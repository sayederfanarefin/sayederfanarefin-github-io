---
layout: about
title: about
permalink: /
subtitle: PhD student, Computer Science, <a href='https://ttu.edu'>Texas Tech University</a>. 

profile:
  align: right
  image: prof_pic.jpg
  address: >
    <p>Computer Science Department</p>
    <p>Texas Tech University</p>

news: true  # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

I am a PhD candidate at Texas Tech University. My advisor is Dr. Abdul Serwadda. My research is focused on Cyber-security. I completed my Bachelors in Computer Science and Engineering in 2017 from BRAC University, Dhaka, Bangladesh. I completed my Masters in Computer Science and Engineering from BRAC University. 

I work as a Grad Part-Time Instructor at Tech. 
I am open to collaborations.
I love music and I play the drums when I get some free time (!)

