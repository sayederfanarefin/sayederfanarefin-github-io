---
layout: page
permalink: /teaching/
title: teaching
description: Teaching experience as Instructor (Lecture & Lab) and Teaching Assistant.
nav: true
---
#### Lab Instructor/Teaching Assistant
I have worked as a Lab instructor and TA for the following courses.

> Data Structures (CS-2413)
> - Spring 2024

> Programming Principles II: C (CS-1412)
> - Spring 2021, Spring 2022 and Fall 2022

> Data Structures (CS-2413)
> - Fall 2021

> Computer Networks (CS-4392)
> - Fall 2020

> CS-4366
> - Fall 2020



#### Instructor
I am/was also the instructor for the following courses.

> Data Structures (CS-2413)
> - Fall 2023

> Object-Oriented Programming (CS-2365)
> - Summer 2023

> Discrete Computational Structures (CS-1382)
> - Spring 2023

> Computer Organization and Assembly Language Programming (CS-3350)
> - Summer 2022

> Computer Organization and Assembly Language Programming (CS-2350)
> - Summer 2021